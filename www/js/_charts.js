var data = {
  "hodina":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
  "narozeni_hod":[5280,3437,3438,3543,3530,3371,3423,4438,8436,6913,6791,6523,6149,6189,5592,4798,4386,4179,3785,3772,3549,3590,3549,3611],
  "mesic":['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
  "narozeni_mes":[9528,8305,9254,9238,9314,9901,10289,9790,9736,9310,8614,8993],
  "den":['pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota', 'neděle'],
  "narozeni_den":[16783,18005,17433,17721,17485,12734,12120]
};

$(function () {

    /**
     * This plugin extends Highcharts in two ways:
     * - Use HTML5 canvas instead of SVG for rendering of the heatmap squares. Canvas
     *   outperforms SVG when it comes to thousands of single shapes.
     * - Add a K-D-tree to find the nearest point on mouse move. Since we no longer have SVG shapes
     *   to capture mouseovers, we need another way of detecting hover points for the tooltip.
     */
    (function (H) {
        var Series = H.Series,
            each = H.each;

        /**
         * Create a hidden canvas to draw the graph on. The contents is later copied over
         * to an SVG image element.
         */
        Series.prototype.getContext = function () {
            if (!this.canvas) {
                this.canvas = document.createElement('canvas');
                this.canvas.setAttribute('width', this.chart.chartWidth);
                this.canvas.setAttribute('height', this.chart.chartHeight);
                this.image = this.chart.renderer.image('', 0, 0, this.chart.chartWidth, this.chart.chartHeight).add(this.group);
                this.ctx = this.canvas.getContext('2d');
            }
            return this.ctx;
        };

        /**
         * Draw the canvas image inside an SVG image
         */
        Series.prototype.canvasToSVG = function () {
            this.image.attr({ href: this.canvas.toDataURL('image/png') });
        };

        /**
         * Wrap the drawPoints method to draw the points in canvas instead of the slower SVG,
         * that requires one shape each point.
         */
        H.wrap(H.seriesTypes.heatmap.prototype, 'drawPoints', function () {

            var ctx = this.getContext();

            if (ctx) {

                // draw the columns
                each(this.points, function (point) {
                    var plotY = point.plotY,
                        shapeArgs,
                        pointAttr;

                    if (plotY !== undefined && !isNaN(plotY) && point.y !== null) {
                        shapeArgs = point.shapeArgs;

                        pointAttr = (point.pointAttr && point.pointAttr['']) || point.series.pointAttribs(point);

                        ctx.fillStyle = pointAttr.fill;
                        ctx.fillRect(shapeArgs.x, shapeArgs.y, shapeArgs.width, shapeArgs.height);
                    }
                });

                this.canvasToSVG();

            } 
        });
        H.seriesTypes.heatmap.prototype.directTouch = false; // Use k-d-tree
    }(Highcharts));

    var start;
    Highcharts.setOptions({
       lang: {
         decimalPoint: "," ,
         months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
        }
    });

    $.get('data/narozeni.csv', function(csv) {

      Highcharts.chart('heatmapa', {

        data: {
            csv: csv,
            parsed: function () {
                start = +new Date();
            }
        },

        chart: {
            type: 'heatmap',
            margin: [60, 10, 80, 50]
        },


        title: {
            text: 'Novorozenci 2015 hodinu po hodině',
            x: 40
        },

        credits: {
            enabled: false
        },

        xAxis: {
            type: 'datetime',
            min: Date.UTC(2015, 0, 1),
            max: Date.UTC(2016, 0, 1),
            labels: {
                align: 'left',
                x: 5,
                y: 14,
                format: '{value:%B}' // long month
            },
            showLastLabel: false,
            tickLength: 16
        },

        yAxis: {
            title: {
                text: null
            },
            labels: {
                format: '{value}:00'
            },
            minPadding: 0,
            maxPadding: 0,
            startOnTick: false,
            endOnTick: false,
            tickPositions: [0, 6, 12, 18, 24],
            tickWidth: 1,
            min: 0,
            max: 23,
            reversed: true
        },

        colorAxis: {
            //stops: [
            //    [0, '#3060cf'],
            //    [0.7, '#c4463a'],
            //    [1, '#c4463a']
            //],
            //min: 0,
            //max: 50,
            minColor: '#EEFFE5',
            maxColor: '#377eb8',
            startOnTick: false,
            endOnTick: false,
            labels: {
                format: '{value}'
            }
        },

        series: [{
            borderWidth: 0,
            nullColor: '#EFEFEF',
            colsize: 24 * 36e5, // one day
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.x:%d. %m. %Y}, {point.y} hod.: <b>{point.value}</b> narození'
            },
            turboThreshold: Number.MAX_VALUE // #3404, remove after 4.0.5 release
        }],

        responsive: {
            rules: [{
              condition: {
                maxWidth: 600
              },
            }]
        }
      });
    });

    Highcharts.chart('hodiny', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Děti narozené v roce 2015: Celkový přehled po hodinách'
      },
      xAxis: {
        categories: data.hodina,
        crosshair: true
      },
      yAxis: {
        title: { text: "Počet narození" },
        labels: {formatter: function() {return this.value} }
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      tooltip: {
        shared: true,
        headerFormat: '<span style="font-size: 10px">{point.key} hodin</span><br>'
      },      
      plotOptions: {
        column: {
          borderWidth: 0,
        }
      },
      series: [{
        name: 'Celkem narozených dětí',
        data: data.narozeni_hod,
        color: '#377eb8',
        negativeColor: '#e41a1c'
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Děti narozené v roce 2015: Přehled po hodinách'
            }
          }
        }]
      }
    });

    Highcharts.chart('mesice', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Děti narozené v roce 2015: Celkový přehled po měsících'
      },
      xAxis: {
        categories: data.mesic,
        crosshair: true
      },
      yAxis: {
        title: { text: "Počet narození" },
        labels: {formatter: function() {return this.value} },
        min: 7500
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },    
      plotOptions: {
        column: {
          borderWidth: 0,
        }
      },
      series: [{
        name: 'Celkem narozených dětí',
        data: data.narozeni_mes,
        color: '#377eb8',
        negativeColor: '#e41a1c'
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Děti narozené v roce 2015: Přehled po měsících'
            }
          }
        }]
      }
    });

    Highcharts.chart('dny', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Děti narozené v roce 2015: Celkový přehled po dnech v týdnu'
      },
      xAxis: {
        categories: data.den,
        crosshair: true
      },
      yAxis: {
        title: { text: "Počet narození" },
        labels: {formatter: function() {return this.value} },
        min: 10000
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },      
      plotOptions: {
        column: {
          borderWidth: 0,
        }
      },
      series: [{
        name: 'Celkem narozených dětí',
        data: data.narozeni_den,
        color: '#377eb8',
        negativeColor: '#e41a1c'
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Děti narozené v roce 2015: Přehled po dnech'
            }
          }
        }]
      }
    });
})