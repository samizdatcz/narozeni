---
title: "Data: O víkendu a v noci se rodí méně dětí. Kvůli císařským řezům"
perex: "Nejvíc novorozenců přichází na svět v létě - a nejméně o sobotách a nedělích. V noci se zase narodí sotva polovina dětí oproti dopoledni. Jde o rozhodnutí matek, nebo si plánováním termínů ulehčují práci porodníci? Podívejte se na podrobná data o porodech v roce 2015."
description: "Nejvíc novorozenců přichází na svět v létě - a nejméně o sobotách a nedělích. V noci se zase narodí sotva polovina dětí oproti dopoledni. Jde o rozhodnutí matek, nebo si plánováním termínů ulehčují práci porodníci? Podívejte se na podrobná data o porodech v roce 2015, která zpracoval Český rozhlas."
authors: ["Michal Zlatkovský"]
published: "28. prosince 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "porody"
socialimg: https://interaktivni.rozhlas.cz/porody/media/socialimg.jpg
libraries: [jquery, "https://interaktivni.rozhlas.cz/tools/jquery/jquery_csv.js", "https://interaktivni.rozhlas.cz/tools/highcharts/5.0.6.min.js", "https://interaktivni.rozhlas.cz/tools/highcharts/data_5.0.6.js", "https://interaktivni.rozhlas.cz/tools/highcharts/heatmap_5.0.6.js"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/data-v-cesku-se-zdvojnasobil-pocet-cisarskych-rezu-muze-za-to-stari-rodicek-nebo-pristup-lekaru--1677259
    title: Data: v Česku se zdvojnásobil počet císařských řezů. Může za to stáří rodiček, nebo přístup lékařů?
    perex: Porodů císařským řezem od počátku nového tisíciletí přibývá. Jejich počet se od roku 2000 v Česku více než zdvojnásobil, vyplývá to z dat národního registru novorozenců, která Českému rozhlasu poskytl Ústav zdravotnických informací a statistiky. 
    image: http://media.rozhlas.cz/_obrazek/2879492--tehotenstvi-ilustracni-foto--1-400x266p0.jpeg
  - link: https://interaktivni.rozhlas.cz/umrti-srdce/
    title: Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu
    perex: Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi.
    image: https://interaktivni.rozhlas.cz/umrti-srdce/media/socimg.png
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/leky-v-datech-na-cem-jsme-zavisli-kolik-utracime--1465581
    title: Léky v datech: Na čem jsme závislí, kolik utrácíme?
    perex: Léků bereme rok od roku víc. Nejde ovšem jen o mediálně propíraná antibiotika nebo vakcíny. Rychleji dnes roste spotřeba antidepresiv, prášků na srdce nebo léků na rakovinu. Jaké léky bereme? Kdo určuje ceny? Kolik zaplatíte vy, kolik pojišťovna a na kolik si přijdou farmaceutické firmy?
    image: http://media.rozhlas.cz/_obrazek/3309708--viagra-a-dalsi-leky-na-erekci--1-600x400p0.jpeg
---

Čas, kdy přichází na svět nejvíc dětí, se za posledních pětadvacet let výrazně změnil. Staletí dlouhý trend častějších jarních porodů ustoupil plánovanému rodičovství a letním novorozencům. A stoupající počet císařských řezů, o kterém Český rozhlas [informoval na začátku prosince](http://www.rozhlas.cz/zpravy/domaci/_zprava/data-v-cesku-se-zdvojnasobil-pocet-cisarskych-rezu-muze-za-to-stari-rodicek-nebo-pristup-lekaru--1677259), má za následek úbytek nočních a víkendových narození. 


<div data-bso=1></div>

<aside class="big">
<div id="hodiny"></div>
</aside>

Jak Český rozhlas zjistil z dat [Ústavu zdravotnických informací a statistiky](http://www.ajog.org/article/S0002-9378%2813%2900146-4/abstract), existují časy, kdy se děti rodí nejčastěji: počítáno po hodinách je to ráno a dopoledne, v týdnu pak mají pracovní dny převahu nad víkendy. Vysvětlení je podle porodníka Petra Křepelky z Ústavu pro péči o matku a dítě jednoduché: dobu narození ovlivňuje neustále stoupající počet plánovaných císařských řezů.

Těmi se dnes rodí každé čtvrté dítě, dvakrát více než v roce 2000. Z téměř 110 tisíc loni narozených dětí [jich císařským řezem přišlo na svět 29 tisíc](http://www.rozhlas.cz/zpravy/domaci/_zprava/data-v-cesku-se-zdvojnasobil-pocet-cisarskych-rezu-muze-za-to-stari-rodicek-nebo-pristup-lekaru--1677259). Jejich počet podle odborníků narůstá jednak kvůli zvyšujícímu se průměrnému věku rodiček, jednak kvůli postojům lékařů. Pokud nedochází k akutním komplikacím, probíhají císařské řezy plánovaně. "Plánovaných řezů je asi polovina," říká Křepelka. "Zákroky se logicky nejčastěji plánují na obvyklou pracovní dobu - nejčastěji dopoledne o pracovních dnech."


<aside class="big">
  <div id="dny"></div>
</aside>

Data jeho slova potvrzují: nejvíce dětí se v roce 2015 narodilo mezi osmou a devátou ráno, nejméně od páté do šesté ranní. Ze dnů v týdnu novorozenci nejčastěji přicházeli na svět v úterý, nejvzácněji pak v neděli. 

Trendy jsou patrné i při pohledu na čísla po měsících. V Česku stejně jako jinde ve světě platí, že největší počet děti se rodí od července do září. Znamená to, že k jejich početí dochází na konci podzimu a na začátku zimy. Možné vysvětlení přinesla [studie amerických porodních lékařů](http://www.ajog.org/article/S0002-9378%2813%2900146-4/abstract): koncentrace spermií je podle ní nejvyšší právě v zimních měsících a na jaře opět klesá. 

Ještě před rokem 1989 se přitom [nejvíce dětí rodilo na jaře](http://relax.lidovky.cz/spartakiadni-orgie-jsou-pryc-proc-se-nejvic-deti-narodi-v-lete-p62-/zajimavosti.aspx?c=A120920_141428_ln-zajimavosti_jkz). Důvodem byly tehdy obvyklé jarní sňatky a následná letní snaha o zplození dítěte. Léto má ve statistikách navrch nad jarem až od roku 1994.

<aside class="big">
  <div id="mesice"></div>
</aside>

Do počtu novorozenců podle ročních období zapadají i rekordní porodní dny. Nejvíce českých dětí narozených v roce 2015, celkem 488, bude mít narozeniny 14. července - datum jejich početí tedy spadá na konec října 2014, kdy si lidé vzhledem k úternímu svátku často brali volno na prodloužený víkend. Nejméně časté loňské datum narození je pak 15. března - tehdy přišlo na svět 182 dětí.

<aside class="big">
  <div id="heatmapa"></div>
</aside>